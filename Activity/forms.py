from django import forms

class Add_Activity(forms.Form):
	error_messagges = {
		'required': "Please fill in this field",
	}

	name_attrs = {
		'class': 'form-group form-control',
		'type': 'text',
		'placeholder': 'name your activity',
	}

	place_attrs = {
		'class': 'form-group form-control',
		'type': 'text',
		'placeholder': 'activity location',
	}

	category_attrs = {
		'class': 'form-group form-control',
		'type': 'text',
		'placeholder': 'activity category',
	}

	tanggal_attrs = {
		'class': 'form-group form-control',
		'type': 'date',
	}

	time_attrs = {
		'class': 'form-group form-control',
		'type': 'time',
	}

	name = forms.CharField(label="Activity Name", required= True, max_length=50, widget=forms.TextInput(attrs=name_attrs))
	place = forms.CharField(label="Location", required= True, widget=forms.TextInput(attrs=place_attrs))
	category = forms.CharField(label="Category", required= True, max_length=20, widget=forms.TextInput(attrs=category_attrs))
	tanggal = forms.DateField(label="Date", required=True, widget=forms.DateInput(attrs=tanggal_attrs))
	time = forms.TimeField(label="Time", required=True, widget=forms.TimeInput(attrs=time_attrs))