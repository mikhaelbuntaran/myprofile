from django.db import models
from datetime import date

# Create your models here.
class Activity(models.Model):
	name = models.CharField(max_length=50)
	place = models.TextField()
	category = models.CharField(max_length=20)
	tanggal = models.DateField(default=date.today)
	time = models.TimeField(default=date.today)

	def __str__(self):
		return "{}".format(self.name)