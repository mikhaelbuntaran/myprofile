from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('addactivity/', views.addactivity, name='addactivity'),
    path('saveactivity/', views.saveactivity, name='saveactivity'),
    path('deleteactivity/', views.deleteactivity, name='deleteactivity'),
]