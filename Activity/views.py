from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import Activity
from .forms import Add_Activity

# Create your views here.
response = {}
html = 'activity.html'

def index(request):
	activities = Activity.objects.all()
	response['activities'] = activities

	html = 'activity.html'
	return render(request, html, response)

def addactivity(request):
	response['addactivity_forms'] = Add_Activity

	html = 'addactivity.html'
	return render(request, html, response)

def saveactivity(request):
	form = Add_Activity(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		response['tanggal'] = request.POST['tanggal']
		response['time'] = request.POST['time']
		activity = Activity(name=response['name'], place=response['place'],
			category=response['category'], tanggal=response['tanggal'], time=response['time'])
		activity.save()
		return HttpResponseRedirect('/activity/')
	else:
		return HttpResponseRedirect('/')

def deleteactivity(request):
	for activity in Activity.objects.all():
		activity.delete()
	return HttpResponseRedirect('/activity/')